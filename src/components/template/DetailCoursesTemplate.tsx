import { Badge, Typography } from "antd";
import { Card } from "components/ui";
import { PATH } from "constant";
import { useEffect } from "react"
import { useSelector } from "react-redux";
import { generatePath, useNavigate, useParams } from "react-router-dom"
import { RootState, useAppDispatch } from "stores";
import {  layDanhMucKhoaHocThunk, layKhoaHocTheoDanhMucThunk } from "stores/quanLyKhoaHoc/thunk"

const DetailCoursesTemplate = () => {
  const {maDanhMuc } = useParams();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const { khoaHocTheoDanhMuc } = useSelector((state: RootState) => state.quanLyKhoaHoc);
  const { danhMucKhoaHoc } = useSelector((state: RootState) => state.quanLyKhoaHoc);
console.log(khoaHocTheoDanhMuc);


  useEffect(() => {
    // Tải danh mục khóa học khi component được mount
    dispatch(layDanhMucKhoaHocThunk());
}, []);


  useEffect(()=>{
    if(maDanhMuc){
      dispatch(layKhoaHocTheoDanhMucThunk({ maDanhMuc, maNhom : 'GP01'
        
      }))
    }
  },[dispatch, maDanhMuc])

  const danhMuc = danhMucKhoaHoc.find((danhMuc) => danhMuc.maDanhMuc === maDanhMuc);
  
  



  return (
    <div className="py-[100px] container lg:container mx-auto">
      <h2 className=" bg-yellow-400 text-black font-500 text-center text-40 lg:container mx-auto">{danhMuc ? danhMuc.tenDanhMuc : 'Không tìm thấy danh mục'}</h2>
      <div className=" my-40 lg:container w-fit grid grid-cols-1 lg:grid-cols-4 sm:grid-cols-2 justify-items-center justify-center gap-y-20 gap-x-20  mx-auto">
              {
                  khoaHocTheoDanhMuc?.map((khoaHoc) => {
                      return <div className="w-68 bg-zinc-200 shadow-md rounded-xl duration-500 hover:scale-105 hover:shadow-xl">
                        <Badge.Ribbon  text=" Sale off -50%" color="red">
                        <Card
                            className="h-full"
                            title={<Typography.Text>{khoaHoc.danhMucKhoaHoc.tenDanhMucKhoaHoc}</Typography.Text>}
                            key={khoaHoc?.maKhoaHoc}
                            hoverable
                            style={{width: 288, height: 445,
                                    margin: "0 auto"}}
                            cover={<img style={{height: 250}} alt="..." src={khoaHoc?.hinhAnh}/>}
                            onClick={() => {
                              navigate(generatePath(PATH.detail, {
                                maKhoaHoc: khoaHoc.maKhoaHoc
                              })
                              )
                            }}
                        >
                            <Card.Meta  title={<Typography.Text>Khóa học: {khoaHoc?.tenKhoaHoc}</Typography.Text> } description={khoaHoc?.moTa?.substring(0,100)}   />
                        </Card>
                        </Badge.Ribbon>
                      </div>
                  })
              }
          </div>
    </div>
  )
}

export default DetailCoursesTemplate