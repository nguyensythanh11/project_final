import { Tabs } from 'components/ui'
import { AccountInfoTab, AccountListStudentTab} from '.';
import { useAuth } from 'hooks';
import ProfileCourses from '../Profile/ProfileCourses';

export const AccountTemplate = () => {
    const { user } = useAuth();
    var items = []
    if(user?.maLoaiNguoiDung == 'GV' ){
        items = [
            {
                label: (
                    <div className="w-[170px] text-left font-bold hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3 text-black">
                        Thông tin tài khoản
                    </div>
                ),
                key: 'accountInfo',
                children: <AccountInfoTab />
            },
            {   
                label: (
                    <div className="w-[170px] text-left font-bold  hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3 text-black">
                        Danh sách người dùng
                    </div>
                ),
                key: 'listStudent',
                children: <AccountListStudentTab />
            },
            {   
                label: (
                    <div className="w-[170px] text-left font-bold  hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3 text-black">
                        Danh sách khóa học
                    </div>
                ),
                key: 'listCourse',
                children: <ProfileCourses />
            },
        ]
    } else {
        items = [
            {
                label: (
                    <div className="w-[170px] text-left font-bold hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3 text-black">
                        Thông tin tài khoản
                    </div>
                ),
                key: 'accountInfo',
                children: <AccountInfoTab />
            },
            {   
                label: (
                    <div className="w-[170px] text-left font-bold  hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3 text-black">
                        Danh sách khóa học
                    </div>
                ),
                key: 'listCourse',
                children: <ProfileCourses />
            },
        ]
    }
    const accessTokenJson = localStorage.getItem('accessToken');
    if(accessTokenJson){
        var accessToken = JSON.parse(accessTokenJson);
    }
    if(!accessToken){
        return;
    };
    return (
        <div className='pt-[90px] pb-[50px] '>
            <Tabs
                tabPosition="left"
                className="h-full"
                tabBarGutter={-5}
                items={items}
            />
        </div>
    )
}

export default AccountTemplate
