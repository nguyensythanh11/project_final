import { useEffect } from "react";
import styles from "./resultSearch.module.scss";
import { FaClock, FaRegCalendar, FaSignal, FaStar } from "react-icons/fa";
import { useSelector } from "react-redux";
import { NavLink, useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "stores";
import { LayDanhSachKhoaHocthunk } from "stores/quanLyKhoaHoc/thunk";
import ButtonV2 from "components/ui/ButtonV2/ButtonV2";
export const ResultSearch = () => {
  const dispatch = useAppDispatch();
  const { tenKhoaHoc } = useParams<{ tenKhoaHoc: string }>();
  const { listCourses } = useSelector(
    (state: RootState) => state.quanLyKhoaHoc
  );

  console.log("BC", listCourses);
  useEffect(() => {
    if (tenKhoaHoc) {
      dispatch(LayDanhSachKhoaHocthunk(tenKhoaHoc));
    }
  }, [dispatch, tenKhoaHoc]);

  return (
    <div className={styles.resultSearch}>
      <h4 className="text-center py-10 pb-10">
        {" "}
        Tìm được <span>{listCourses.length}</span> khóa học
      </h4>
      <div className={styles.listResult}>
        <div className="lg:container w-fit grid grid-cols-1 lg:grid-cols-2 sm:grid-cols-1 justify-items-center justify-center gap-20  mx-auto">
          {listCourses?.map((item) => {
            return (
              <div className={styles.itemResult}>
                <div className={styles.imageCourse}>
                  <img src={item.hinhAnh} alt="" />
                </div>
                <div className={styles.courseContent}>
                  <h4>{item.tenKhoaHoc}</h4>
                  <p className={styles.courseDes}>{item.moTa}</p>
                  <div className={styles.iconTime}>
                    <div>
                      <FaClock />
                      <span>8 giờ</span>
                    </div>
                    <div>
                      <FaRegCalendar />
                      <span>23 giờ</span>
                    </div>
                    <div>
                      <FaSignal />
                      <span>All level</span>
                    </div>
                  </div>
                  <div className={styles.iconStar}>
                    <span>Đánh giá :</span>
                    <FaStar />
                    <FaStar />
                    <FaStar />
                    <FaStar />
                    <FaStar />
                  </div>
                  <div className={styles.infoMaker}>
                    <img
                      src="https://demo2.cybersoft.edu.vn/static/media/avatar2.bb9626e2.png"
                      alt=""
                    />
                    <p>{item.nguoiTao.hoTen}</p>
                    <div className="ml-10 absolute bottom-4 right-4">
                      <NavLink to={`/detail/${item.maKhoaHoc}`}>
                        <ButtonV2 className={styles.btn}>Xem chi tiết</ButtonV2>
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ResultSearch;
