import styles from './filterSearch.module.scss'
import { FaCheck, FaStar } from 'react-icons/fa';

export const  FilterSearch = () => {
  return (
    <div className={styles.filterSearch}>
        <h2>Filter</h2>
        <div className={styles.filterItem}>
          <h4>Courses</h4>
          <ul>
            <li>
              <label className={styles.BoxSearch}>
                ALL
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                    <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
                Front End
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                    <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
                Back End
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                    <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
                FullStack
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                    <FaCheck/>
                </span>
              </label>
            </li>
          </ul>
        </div>
        <div className={styles.filterItem}>
          <h4>Level</h4>
          <ul>
            <li>
              <label className={styles.BoxSearch}>
                ALL
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                    <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
                Get Start
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                    <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
              Middle level
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                    <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
              High level
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                    <FaCheck/>
                </span>
              </label>
            </li>
          </ul>
        </div>
        <div className={styles.filterItem}>
          <h4>Đánh giá</h4>
          <ul>
            <li>
              <label className={styles.BoxSearch}>
                <FaStar/>
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                  <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
                <FaStar/>
                <FaStar/>
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                  <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
                <FaStar/>
                <FaStar/>
                <FaStar/>
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                  <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
                <FaStar/>
                <FaStar/>
                <FaStar/>
                <FaStar/>
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                  <FaCheck/>
                </span>
              </label>
            </li>
            <li>
              <label className={styles.BoxSearch}>
                <FaStar/>
                <FaStar/>
                <FaStar/>
                <FaStar/>
                <FaStar/>
                <input type='checkbox'/>
                <span className={styles.checkMark}>
                  <FaCheck/>
                </span>
              </label>
            </li>
          </ul>
        </div>
    </div>
  )
}

export default FilterSearch
