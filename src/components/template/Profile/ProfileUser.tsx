import { Input, InputForm } from "components/ui";
import styles from "./ProfileUser.module.scss";
import { useAuth } from "hooks";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { styled } from "styled-components";
import {
  FaUserClock,
  FaLayerGroup,
  FaSwatchbook,
  FaSignal,
  FaGraduationCap,
  FaBook,
} from "react-icons/fa";
import { Button, Form, Modal, Progress } from "antd";
import ButtonV1 from "components/ui/ButtonV1/ButtonV1";
import { setSpinnerOff, setSpinnerOn } from "stores/spinnerSlice";
import { quanLyNguoiDungServices } from "services";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "stores";
import { toast } from "react-toastify";

export const fetchInfoAccount = (dispatch) => {
  const { user } = useSelector((state: RootState) => state.quanLyNguoiDung);

  

  let data = {
    taiKhoan: user?.taiKhoan,
  };
  dispatch(setSpinnerOn());
  quanLyNguoiDungServices
    .layThongTinNguoiDung(data)
    .then(() => {
      dispatch(setSpinnerOff());
      // dispatch(layThongTinNguoiDung(res.data))
    })
    .catch((err) => {
      dispatch(setSpinnerOff());
      console.log(err);
    });
};

export const ProfileUser = () => {
  const { user } = useAuth();
  const dispatch  = useAppDispatch()

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isEditMode, setIsEditMode] = useState(false); // Thêm state để xác định chế độ chỉnh sửa
  const [formData, setFormData] = useState(user);

  useEffect(() => {
    // Kiểm tra nếu formData có dữ liệu, tức là sau khi cập nhật thành công
    // Đóng modal và reset formData để tránh việc hiển thị dữ liệu cũ trong modal
    if (formData && Object.keys(formData).length > 0) {
      setIsModalVisible(false);
      setIsEditMode(false);
    }
  }, [formData]);

  const showModal = () => {
    setIsModalVisible(true);
    setIsEditMode(true); // Bật chế độ chỉnh sửa khi hiển thị modal
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setIsEditMode(false); // Tắt chế độ chỉnh sửa khi đóng modal
    reset();
  };

 

  const { reset, register } = useForm();

  useEffect(() => {
    reset(user);
  }, [user, reset]);

  const onFinish = (values) => {
    const data = {
      taiKhoan: user.taiKhoan,
      matKhau: values.matKhau,
      hoTen: values.hoTen,
      soDT: values.soDT,
      maLoaiNguoiDung: user.maLoaiNguoiDung,
      maNhom: user.maNhom,
      email: values.email,
    };
    dispatch(setSpinnerOn());
    quanLyNguoiDungServices.capNhatThongTinNguoiDung(data)
      .then((res) => {
        dispatch(setSpinnerOff());
        // Cập nhật thông tin người dùng ở đây
        setFormData(res.data); 
        
        reset(res.data); // Reset form với dữ liệu mới
        toast.success('Cập nhật thành công!');
      })
      .catch((err) => {
        dispatch(setSpinnerOff());
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

 

  return (
    <div className="w-fit grid grid-cols-1 lg:grid-cols-2 sm:grid-cols-1 justify-items-center justify-center gap-20 py-20 lg:container mx-auto">
      
      <div className="mr-[100px] w-full">
      <h2 className=" uppercase pl-[36px] text-gray-600  text-26">
        Thông tin Tài khoản
      </h2>
      <form className="px-40 w-full grid grid-cols-1 lg:grid-cols-2 sm:grid-cols-1  justify-center gap-20 lg:container  mx-auto ">
        <InputS
          disable={!isEditMode}
          label="Tài khoản"
          name="taiKhoan"
          register={register}
        />
        <InputS
          disable={!isEditMode}
          label="Họ và tên"
          name="hoTen"
          register={register}
        />
        <InputS
          disable={!isEditMode}
          label="Số điện thoại"
          name="soDT"
          register={register}
        />
        <InputS
          disable={!isEditMode}
          label="Email"
          name="email"
          register={register}
        />
        <InputS
          disable={!isEditMode}
          label="Mã nhóm"
          name="maNhom"
          register={register}
        />
        <InputS
          disable={!isEditMode}
          label="Loại người dùng"
          name="maLoaiNguoiDung"
          register={register}
        />
      </form>
      <div className="px-40 ">
        {" "}
        <ButtonV1 onClick={showModal}>Edit User</ButtonV1>{" "}
      </div>
      <Modal
        title="Update User"
        visible={isModalVisible}
        onCancel={handleCancel}
       
            footer={[
                <Button key="cancel" onClick={handleCancel}>
                  Hủy
                </Button>,
                
              ]
        } // Để không hiển thị footer modal
      >
        <Form
             className={styles.formBody}
             name="basic1"
             labelCol={{
               span: 0,
             }}
             wrapperCol={{
               span: 24,
             }}
             initialValues={{
               hoTen: user?.hoTen,
               taiKhoan: user?.taiKhoan,
               email: user?.email,
              //  maNhom: formData.maNhom,
               matKhau: user?.matkhau,
            //    maLoaiNguoiDung: user?.maLoaiNguoiDung,
               soDT: user?.soDT,

               // Các giá trị khác tương tự
             }}
             onFinish={onFinish}
             onFinishFailed={onFinishFailed}
             autoComplete="off"
             layout='vertical'
             requiredMark={false}
          >

            {/* username */}
            <Form.Item
              className={styles.formItem}
              // label="Họ và tên"
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: 'Tên không được để trống!',
                },
                {
                  pattern: /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]{3,}$/,
                  message: "Tên phải là chữ và ít nhất 3 ký tự!",
                },
              ]}

            >
              <Input placeholder='Họ tên' className={styles.typeInput} />
            </Form.Item>

            {/* passs */}
            <Form.Item
              className={styles.formItem}
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Mật khẩu không được để trống',
                },
                {
                  pattern: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[_#?!@$%^&*()-]).{8,}$/,
                  message: "Mật khẩu ít nhất 8 ký tự, ít nhất 1 kí tự, 1 chữ và 1 số!",
                },
              ]}
            >
              <Input placeholder='Mật khẩu' className={styles.typeInput} />
            </Form.Item>

            {/* email */}
            <Form.Item
              className={styles.formItem}
              // label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Email không được để trống!',
                },
                {
                  pattern:
                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                  message: "Email không hợp lệ!",
                },
              ]}
            >
              <Input placeholder='Email' className={styles.typeInput} />
            </Form.Item>

            {/* phonenumber */}
            <Form.Item
              className={styles.formItem}
              // label="Số điện thoại"
              name="soDT"
              rules={[
                {
                  required: true,
                  message: 'Số điện thoại không được để trống!',
                },
                {
                  pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
                  message: "Số điện thoại không hợp lệ!",
                },
              ]}
            >
              <Input placeholder='Số điện thoại' className={styles.typeInput} />
            </Form.Item>

            {/* btn */}
            <Form.Item
              className={styles.formBtn}
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button type="primary" className={styles.btnUpdate} htmlType="submit">
                Cập nhật
              </Button>
            </Form.Item>
          </Form>
        
      </Modal>
      </div>
      <div className={styles.infoBot}>
        <h4>KĨ NĂNG CỦA TÔI</h4>
        <div className={styles.infoBotContent}>
          <div className={styles.skillAll}>
            <div className={styles.mySkill}>
              <button className={styles.skillBtn}>HTML</button>
              <Progress
                showInfo={false}
                percent={100}
                strokeColor={{
                  "0%": "#41b294",
                  "100%": "#f9ca9a",
                }}
              />
            </div>
            <div className={styles.mySkill}>
              <button className={styles.skillBtn}>CSS</button>
              <Progress
                showInfo={false}
                percent={75}
                strokeColor={{
                  "0%": "#41b294",
                  "100%": "#f8bebb",
                }}
              />
            </div>
            <div className={styles.mySkill}>
              <button className={styles.skillBtn}>JS</button>
              <Progress
                showInfo={false}
                percent={50}
                strokeColor={{
                  "0%": "#41b294",
                  "100%": "#f0cc6b",
                }}
              />
            </div>
            <div className={styles.mySkill}>
              <button className={styles.skillBtn}>REACT</button>
              <Progress
                showInfo={false}
                percent={80}
                strokeColor={{
                  "0%": "#41b294",
                  "100%": "#113d3c",
                }}
              />
            </div>
          </div>
          <div className="w-fit grid grid-cols-2 lg:grid-cols-3 sm:grid-cols-2  justify-center gap-20 py-20 lg:container mx-auto">
            <div className={styles.timeStudyItem}>
              <FaUserClock />
              <div>
                <h6>Giờ học</h6>
                <p>250</p>
              </div>
            </div>
            <div className={styles.timeStudyItem}>
              <FaLayerGroup />
              <div>
                <h6>Điểm tổng</h6>
                <p>80</p>
              </div>
            </div>
            <div className={styles.timeStudyItem}>
              <FaSwatchbook />
              <div>
                <h6>Buổi học</h6>
                <p>48</p>
              </div>
            </div>
            <div className={styles.timeStudyItem}>
              <FaSignal />
              <div>
                <h6>Cấp độ</h6>
                <p>Fresher</p>
              </div>
            </div>
            <div className={styles.timeStudyItem}>
              <FaGraduationCap />
              <div>
                <h6>Học lực</h6>
                <p>Khá</p>
              </div>
            </div>
            <div className={styles.timeStudyItem}>
              <FaBook />
              <div>
                <h6>Bài tập</h6>
                <p>50</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileUser;

const InputS = styled(InputForm)`
  margin-top: 20px;
  input {
    background-color: transparent !important;
    border: 1px solid black;
    color: black;
  }
`;
