import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, Input, Modal, message } from 'antd';
import styles from './formUpdateInfo.module.scss';
import { setSpinnerOff, setSpinnerOn } from 'stores/spinnerSlice';
import { RootState } from 'stores';
import { quanLyNguoiDungServices } from 'services';

interface FormUpdateInfoProps {
  visible: boolean;
  onCancel: () => void;
  onUpdate: (values: any) => void;
}

export const FormUpdateInfo: React.FC<FormUpdateInfoProps> = ({} ) => {
  const dispatch = useDispatch();
  const { user } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const [, setFormData] = useState(user); // Sử dụng useState để lưu trữ dữ liệu form

  const onFinish = (values) => {
    const data = {
      taiKhoan: user.taiKhoan,
      matKhau: values.matKhau,
      hoTen: values.hoTen,
      soDT: values.soDT,
      maLoaiNguoiDung: user.maLoaiNguoiDung,
      maNhom: user.maNhom,
      email: values.email,
    };
    dispatch(setSpinnerOn());
    quanLyNguoiDungServices.capNhatThongTinNguoiDung(data)
      .then((res) => {
        dispatch(setSpinnerOff());
        // Cập nhật thông tin người dùng ở đây
        setFormData(res.data); 
        message.success('Cập nhật thành công!');
      })
      .catch((err) => {
        dispatch(setSpinnerOff());
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };


  const [isModalVisible, setIsModalVisible] = useState(false);
  const [_, setIsEditMode] = useState(false); // Thêm state để xác định chế độ chỉnh sửa

  const handleCancel = () => {
    setIsModalVisible(false);
    setIsEditMode(false); // Tắt chế độ chỉnh sửa khi đóng modal
  };

  const handleUpdate = (values) => {
    // Xử lý logic cập nhật thông tin người dùng ở đây
    console.log("Thông tin cập nhật:", values);
    setIsEditMode(false); // Tắt chế độ chỉnh sửa sau khi cập nhật
  };

  return (<Modal
  title="Update User"
  visible={isModalVisible}
  onCancel={handleCancel}
 
      footer={[
          <Button key="cancel" onClick={handleCancel}>
            Hủy
          </Button>,
          <Button key="update" type="primary" onClick={handleUpdate}>
            Cập nhật
          </Button>,
        ]
  } 
  >
  {user ? (
    <div className={styles.formUpdate}>
      {/* ... */}

      <Form
             className={styles.formBody}
             name="basic1"
             labelCol={{
               span: 0,
             }}
             wrapperCol={{
               span: 24,
             }}
             initialValues={{
               hoTen: user?.hoTen,
               taiKhoan: user?.taiKhoan,
               email: user?.email,
              //  maNhom: formData.maNhom,
               matKhau: user?.matkhau,
              //  maLoaiNguoiDung: formData.maLoaiNguoiDung,
               soDT: user?.soDT,

               // Các giá trị khác tương tự
             }}
             onFinish={onFinish}
             onFinishFailed={onFinishFailed}
             autoComplete="off"
             layout='vertical'
             requiredMark={false}
          >

            {/* username */}
            <Form.Item
              className={styles.formItem}
              // label="Họ và tên"
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: 'Tên không được để trống!',
                },
                {
                  pattern: /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]{3,}$/,
                  message: "Tên phải là chữ và ít nhất 3 ký tự!",
                },
              ]}

            >
              <Input placeholder='Họ tên' className={styles.typeInput} />
            </Form.Item>

            {/* passs */}
            <Form.Item
              className={styles.formItem}
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Mật khẩu không được để trống',
                },
                {
                  pattern: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[_#?!@$%^&*()-]).{8,}$/,
                  message: "Mật khẩu ít nhất 8 ký tự, ít nhất 1 kí tự đặt biệt, 1 chữ in hoa, 1 chữ thường và 1 số!",
                },
              ]}
            >
              <Input.Password placeholder='Mật khẩu' className={styles.typeInput} />
            </Form.Item>

            {/* email */}
            <Form.Item
              className={styles.formItem}
              // label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: 'Email không được để trống!',
                },
                {
                  pattern:
                    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                  message: "Email không hợp lệ!",
                },
              ]}
            >
              <Input placeholder='Email' className={styles.typeInput} />
            </Form.Item>

            {/* phonenumber */}
            <Form.Item
              className={styles.formItem}
              // label="Số điện thoại"
              name="soDT"
              rules={[
                {
                  required: true,
                  message: 'Số điện thoại không được để trống!',
                },
                {
                  pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
                  message: "Số điện thoại không hợp lệ!",
                },
              ]}
            >
              <Input placeholder='Số điện thoại' className={styles.typeInput} />
            </Form.Item>

            {/* btn */}
            <Form.Item
              className={styles.formBtn}
              wrapperCol={{
                offset: 0,
                span: 24,
              }}
            >
              <Button type="primary" className={styles.btnUpdate} htmlType="submit">
                Cập nhật
              </Button>
            </Form.Item>
          </Form>
    </div>
  ) : (<></>)}
  </Modal>)

};

export default FormUpdateInfo;
