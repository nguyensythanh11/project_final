import { useState } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate } from "react-router-dom";
import { RootState, useAppDispatch } from "stores";
import {
  deleteGhiDanhKhoaHocThunk,
} from "stores/quanLyKhoaHoc/thunk";

import { toast } from "react-toastify";
import { ChiTietKhoaHocGhiDanh } from "types";
import { Button, Input } from "components/ui";
import { PATH } from "constant";


const { Search } = Input;
const ProfileCourses = () => {
  const { user } = useSelector((state: RootState) => state.quanLyNguoiDung);
  const taiKhoan = useSelector(
    (state: RootState) => state.quanLyNguoiDung.user.taiKhoan
  );
  const ghiDanh = user.chiTietKhoaHocGhiDanh;

  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [listCourse, setListCourse] = useState(() => ghiDanh);

  const haldeDeleteCourse = (maKhoaHoc) => {
    const khoaHocDaGhiDanh = ghiDanh.find(
      (item) => item.maKhoaHoc === maKhoaHoc
    );

    if (khoaHocDaGhiDanh) {
      dispatch(deleteGhiDanhKhoaHocThunk({ maKhoaHoc, taiKhoan }))
        .unwrap()
        .then(() => {
          const updatedList = listCourse.filter(
            (item) => item.maKhoaHoc !== maKhoaHoc
          );
          setListCourse(updatedList);

          toast.success("Bạn đã hủy ghi danh thành công");
        })
        .catch((error) => {
          toast.error(error);
        });
    }
  };

  const onChangeSearch = (e) => {
    let value = e.target.value.toLowerCase();
    let newList = [...ghiDanh];
    let newSearch = newList.filter((item) => {
      return item.tenKhoaHoc.toLowerCase().includes(value);
    });
    setListCourse(newSearch);
  };

  return (
    <div >
      <div className="flex items-center  justify-center">
        <h2 className="uppercase pl-[36px]  text-gray-600 text-26 mr-50">Thông tin khóa học ghi danh</h2>
        <Search
          placeholder="Tìm kiếm"
          onChange={onChangeSearch}
          style={{
            width: 300,
          }}
        />
      </div>
      <div className="lg:container w-fit grid grid-cols-1 lg:grid-cols-2 sm:grid-cols-1 justify-items-center justify-center gap-20  mx-auto">
        {listCourse?.map((item: ChiTietKhoaHocGhiDanh) => {
          return (
            <div
              key={item.maKhoaHoc}
              className="flex my-20 w-[500px]  rounded border  border-yellow-300 mx-auto"
            >
              <img
                className=""
                style={{ width: 200, height: 200 }}
                alt="..."
                src={item?.hinhAnh}
              />
              <div className=" ml-[20px] relative">
                <h3 className="text-20 font-semibold">
                  {" "}
                  Tên khóa học: {item.tenKhoaHoc}
                </h3>
                <p>
                  <b>Desctription:</b> {item.moTa.substring(0, 200)}
                </p>
                <p>
                  <b>Đánh giá: </b>
                  {item.danhGia}
                </p>
                <p>
                  <b>Lượt xem: </b>
                  {item.luotXem}
                </p>
                <p className="pb-40">
                  <b >Ngày tạo: </b>
                  {item.ngayTao.substring(0, 10)}
                </p>
                <div className="absolute bottom-4 right-4">
                  <Button
                    className="mr-10"
                    onClick={() => {
                      navigate(
                        generatePath(PATH.detail, {
                          maKhoaHoc: item.maKhoaHoc,
                        })
                      );
                    }}
                  >
                    Xem khóa học
                  </Button>
                  <Button onClick={() => haldeDeleteCourse(item.maKhoaHoc)}>
                    Hủy
                  </Button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default ProfileCourses;
