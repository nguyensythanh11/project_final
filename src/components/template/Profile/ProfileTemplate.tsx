import { Tabs } from "components/ui/antDesign"
import ProfileUser from "./ProfileUser"
import ProfileCourses from "./ProfileCourses"

const ProfileTemplate = () => {
  const accessTokenJson = localStorage.getItem('accessToken');
    if(accessTokenJson){
        var accessToken = JSON.parse(accessTokenJson);
    }
    if(!accessToken) return;
  return (
    <div className=' py-[100px] w-11/12 mx-auto'>
            <h2 className=' uppercase text-center text-zinc-500 font-bold text-4xl pb-30'>Thông tin người dùng</h2>
            <Tabs
                tabPosition="top"
                className="h-full "
                tabBarGutter={-5}
                items={[
                    {
                        label: (
                            <div className="w-[150px] text-left font-bold text-gray-600 hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3">
                                Thông tin tài khoản
                            </div>
                        ),
                        key: 'accountInfo',
                        children: <ProfileUser />,
                    },
                    {
                        label: (
                            <div className="w-[150px] text-left font-bold  hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3  text-gray-600">
                                Thông tin khóa học
                            </div>
                        ),
                        key: 'CourseInfo',
                        children: <ProfileCourses />,
                    },
                ]}
            />
        </div>
  )
}

export default ProfileTemplate