import { Modal as ModalA, ModalProps as ModalPropsA } from 'antd'

export const Modal = (props: ModalPropsA) => {
  return (
    <ModalA {... props}></ModalA>
  )
}

export default Modal