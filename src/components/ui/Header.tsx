import styled from "styled-components";
import { Avatar, Input, Popover } from ".";
import {
  ShoppingCartOutlined,
  GlobalOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { NavLink, useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { useAuth } from "hooks";
import { FaAlignLeft } from "react-icons/fa";
import { useEffect, useState } from "react";
import { RootState, useAppDispatch } from "stores";
import {
  layDanhMucKhoaHocThunk,
  layKhoaHocTheoDanhMucThunk,
} from "stores/quanLyKhoaHoc/thunk";
import { useSelector } from "react-redux";
import { quanLyNguoiDungActions } from "stores/quanLyNguoiDung/slice";

export const Header = () => {
  const navigate = useNavigate();
  const { user } = useAuth();
  const dispatch = useAppDispatch();
  const [maDanhMuc, _] = useState("");
  const accessTokenJson = localStorage.getItem("accessToken");
  const { danhMucKhoaHoc } = useSelector(
    (state: RootState) => state.quanLyKhoaHoc
    );
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);

  const toggleMobileMenu = () => {
    setIsMobileMenuOpen(!isMobileMenuOpen);
  };

  
  useEffect(() => {
    dispatch(layDanhMucKhoaHocThunk());
  }, [dispatch]);

  useEffect(() => {
    if (maDanhMuc) {
      dispatch(
        layKhoaHocTheoDanhMucThunk({
          maDanhMuc: maDanhMuc,
          maNhom: user?.maNhom,
        })
      );
    }
  }, [dispatch, maDanhMuc]);

  const onSearch = (tenKhoaHoc: string) => {
    if (tenKhoaHoc) {
      navigate(`${PATH.searchPage.replace(":tenKhoaHoc", tenKhoaHoc)}`);
    }
  };

  const content = (
    <ul className="flex flex-col w-[180px] ">
      {danhMucKhoaHoc?.map((khoaHoc) => (
        <NavLink
          to={PATH.detailCourses.replace(":maDanhMuc", khoaHoc.maDanhMuc)}
          className="hover:cursor-pointer !text-black  hover:bg-zinc-400 hover:!text-white text-16"
          key={khoaHoc.maDanhMuc}
        >
          {khoaHoc.tenDanhMuc}
        </NavLink>
      ))}
    </ul>
  );

  return (
    <HeaderS>
      <nav className="bg-white border-gray-200 px-4 lg:px-6 py-2.5 dark:bg-gray-800 space-x-20">
        <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
          <a href="/" className="flex items-center">
            <img
              src="https://www.udemy.com/staticx/udemy/images/v7/logo-udemy.svg"
              alt="Udemy"
              className="mr-3 h-[60px] w-[60px] sm:h-[60px] sm:w-[60px]"
              loading="lazy"
            />
          </a>
          <div className="flex items-center lg:order-2">
            <button
            onClick={toggleMobileMenu}
              type="button"
              className="inline-flex items-center p-2 ml-1 text-sm text-gray-500 rounded-lg hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
              aria-controls="mobile-menu"
              aria-expanded="false"
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className="w-32 h-32"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clipRule="evenodd"
                ></path>
              </svg>
              <svg
                className="hidden w-6 h-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </button>
          </div>
          <div
            className=" fix-nav bg-white justify-between items-center w-full lg:flex lg:w-auto lg:order-1"
            
          >
          
          {!isMobileMenuOpen && (
            <div
            className="mobile-menu justify-between items-center w-full lg:flex lg:w-auto lg:order-1"
            id="mobile-menu"
          >
            <div className="flex flex-col mt-4 w-full font-medium lg:flex-row lg:space-x-8 lg:mt-0">
              <div className="leading-[50px] icon-fix pr-20 block ">
                <Popover
                  content={content}
                  className="content flex items-center justify-center cursor-pointer"
                >
                  <span className="pr-3 font-600">Danh Mục</span>{" "}
                  <FaAlignLeft />
                </Popover>
              </div>
                <div className="  lg:flex lg:flex-1 lg:justify-end flex items-center pr-20">
                  <Input.Search
                    className="inputSearch ml-2"
                    placeholder="Tìm kiếm"
                    onSearch={onSearch}
                    style={{
                      maxWidth: 250,
                    }}
                  />
                </div>
              <div className="block icon-fix leading-[50px] pr-20">
                <Popover
                  content={
                    <div className="flex flex-col justify-center items-center">
                      <p
                        className="text-center mb-10"
                        style={{
                          width: 300,
                        }}
                      >
                        Cho phép nhóm của bạn truy cập vào hơn 22.000 khóa học
                        hàng đầu của Udemy, ở mọi nơi và mọi lúc.
                      </p>
                      <button
                        style={{
                          width: 200,
                          height: 50,
                        }}
                        className="bg-black hover:bg-gray-600 transition-all duration-300 text-white font-bold py-2 px-4 rounded"
                      >
                        Dùng thử Udemy Business
                      </button>
                    </div>
                  }
                  className="content font-500 cursor-pointer"
                >
                  Udemy Business
                </Popover>
              </div>
              <div className="block icon-fix leading-[50px] pr-20">
                <Popover
                  className="content font-500 cursor-pointer"
                  content={
                    <div>
                      <p>
                        Biến kiến thức của bạn thành cơ hội và tiếp cận với hàng
                        triệu người trên thế giới.
                      </p>
                      <button className="bg-black hover:bg-gray-600 transition-all duration-300 text-white font-bold py-2 px-4 rounded">
                        Dùng thử Udemy Business
                      </button>
                    </div>
                  }
                >
                  Giảng dạy trên Udemy
                </Popover>
              </div>
              <div className="block icon-fix leading-[50px] pr-20">
                {accessTokenJson && (
                  <Popover
                    className="content font-500 cursor-pointer"
                    content={
                      <div>
                        <button className="bg-black hover:bg-gray-600 transition-all duration-300 text-white font-bold py-2 px-4 rounded">
                          Chuyển đến Quá trình học của tui
                        </button>
                      </div>
                    }
                  >
                    Quá trình học tập của tôi
                  </Popover>
                )}
              </div>
              <div className="block icon-fix leading-[50px] pr-20">
                <ShoppingCartOutlined className="text-26 font-600 cursor-pointer" />
              </div>
              <div className="icon-fix flex items-center justify-center">
                <div className="flex space-x-10 ">
                  {!accessTokenJson && (
                    <button
                      onClick={() => {
                        navigate(PATH.login);
                      }}
                      className="text-14 bg-white hover:bg-gray-400 text-black font-bold py-8 px-8 border border-black"
                    >
                      Đăng nhập
                    </button>
                  )}
                  {!accessTokenJson && (
                    <button
                      onClick={() => {
                        navigate(PATH.register);
                      }}
                      className="text-14 bg-black hover:bg-gray-600 text-white font-bold py-8 px-12 border border-black rounded"
                    >
                      Đăng ký
                    </button>
                  )}
                  {!accessTokenJson && (
                    <div className="border border-black flex items-center">
                      <GlobalOutlined className="p-10" />
                    </div>
                  )}
                  {accessTokenJson && (
                    <Popover
                      content={
                        <div className="p-2 ">
                          <h2 className="font-600 mb-4 p-2">{user?.hoTen}</h2>
                          <hr />

                          {user?.maLoaiNguoiDung === "GV" && (
                            <div
                              onClick={() => {
                                window.location.href = PATH.adminCourse;
                              }}
                              className="p-2 mt-4 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                            >
                              Trang Admin
                            </div>
                          )}
                          <div
                            onClick={() => {
                              window.location.href = PATH.profile;
                            }}
                            className="p-2 mt-4 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                          >
                            Thông tin tài khoản
                          </div>
                          <div
                            className="p-2 mt-4 hover:cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300"
                            onClick={() => {
                              dispatch(quanLyNguoiDungActions.logOut());
                              navigate(PATH.login);
                            }}
                          >
                            Đăng xuất
                          </div>
                        </div>
                      }
                      trigger={"click"}
                    >
                      <Avatar
                        className="!ml-12 hover:cursor-pointer !flex !items-center !justify-center"
                        size={40}
                        icon={<UserOutlined></UserOutlined>}
                      ></Avatar>
                    </Popover>
                  )}
                </div>{" "}
              </div>
            </div>
          </div>
          )}
          </div>
        </div>
      </nav>
    </HeaderS>
  );
};

export default Header;

const HeaderS = styled.header`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  background: white;
  z-index: 50;

  padding: 0 90px;
  height: var(--header-height);
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.08), 0 4px 12px rgba(0, 0, 0, 0.08);
  nav {
    max-width: 90%;
    margin: auto;
    height: 100%;
  }

  @media screen and (max-width: 768px) {
  
  
  
    nav {
    .fix-nav {
    position: absolute;
    top: 90px;
    right: 10px;
    width: 250px;
      
    }
    .icon-fix {
      display: block;
    margin: 0 auto;
    padding: 0;


    }
  }}
  
`;
