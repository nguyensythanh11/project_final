import styles from './buttonV1.module.scss';

export const ButtonV1 = (props) => {
  return (
    <button onClick={props.onClick} className={styles.btnStyle}>{props.children}</button>
  )
}

export default ButtonV1