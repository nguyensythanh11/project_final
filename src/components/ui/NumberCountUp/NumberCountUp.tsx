import styles from './NumberCountUp.module.scss'
import student from '/images/studentsNumber.png'
import timeTable from '/images/timetableNumber.png'
import hourglass from '/images/hourglassNumber.png'
import teacher from '/images/teacherNumber.png'
import CountUp from 'react-countup';

export const NumberCountUp = () => {
  return (
    <div className={styles.numberCountUp}>
        <div className="myContainer">
            <div className={styles.numberContainer}>
                <div className={styles.numberItem}>
                    <div>
                        <img src={student} alt="" />
                    </div>
                    <div>
                        <span className={styles.textNumber}>
                            <CountUp 
                            end={1050} 
                            duration={3}
                            enableScrollSpy={true}
                            />
                        </span>
                    </div>
                    <p className={styles.textNumberTitle}>Học viên</p>
                </div>
                <div className={styles.numberItem}>
                    <div>
                        <img src={timeTable} alt="" />
                    </div>
                    <div>
                        <span className={styles.textNumber}>
                        <CountUp 
                            end={1500} 
                            duration={3}
                            enableScrollSpy={true}
                            />
                        </span>
                    </div>
                    <p className={styles.textNumberTitle}>Khóa học</p>
                </div>
                <div className={styles.numberItem}>
                    <div>
                        <img src={hourglass} alt="" />
                    </div>
                    <div>
                        <span className={styles.textNumber}>
                            <CountUp 
                            end={35000} 
                            duration={3}
                            enableScrollSpy={true}
                            />
                        </span>
                    </div>
                    <p className={styles.textNumberTitle}>Giờ học</p>
                </div>
                <div className={styles.numberItem}>
                    <div>
                        <img src={teacher} alt="" />
                    </div>
                    <div>
                        <span className={styles.textNumber}>
                            <CountUp 
                            end={350} 
                            duration={3}
                            enableScrollSpy={true}
                            />
                        </span>
                    </div>
                    <p className={styles.textNumberTitle}>Giảng viên</p>
                </div>
            </div>
        </div>
    </div>
  )
}


export default NumberCountUp