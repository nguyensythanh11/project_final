import styles from './buttonV2.module.scss';

export const ButtonV2 = (props) => {
  return (
    <button onClick={props.onclick} className={styles.btnStyle}>{props.children}</button>
  )
}

export default ButtonV2
