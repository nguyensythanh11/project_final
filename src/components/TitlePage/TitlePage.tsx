import styles from './titlePage.module.scss'
export const TitlePage = (props) => {
  return (
    <div className={styles.titlePage}>
        <div className="myContainer">
            <h3>{props.title}</h3>
            <p>{props.text}</p>
        </div>
    </div>
  )
}

export default TitlePage
