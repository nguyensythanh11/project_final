import { Footer, Header } from 'components/ui'
import { Outlet, Navigate } from 'react-router-dom'

export const AuthLayout = () => {
  const accessTokenJson = JSON.parse(localStorage.getItem('accessToken'));
  if(accessTokenJson){
      return <Navigate to='/'></Navigate>;
  }

  return (
    <div className="AuthLayout w-full h-full overflow-hidden">
        <Header />
        <div className="h-screen w-screen relative">
            <div className="absolute top-0 left-0 w-full h-full z-10"></div>
            <div className="absolute w-[450px] p-[30px] top-1/2 left-1/2 z-20 -translate-x-1/2 -translate-y-1/2 rounded-md">
                <Outlet />
            </div>
        </div>
        <Footer />
    </div>
  )
}

export default AuthLayout