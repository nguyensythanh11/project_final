import { apiInstance } from "constant"
import { DanhMucKhoaHoc, DanhSachKhoaHoc_PhanTrang, KhoaHocTheoDanhMuc, QueryLayKhoaHocTheoDanhMuc, danhSachKhoaHoc, ghiDanhKH, listHVKH } from "types"
import { generateSearchString } from "utils";

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_KHOA_HOC_API
})

export const quanLyKhoaHocServices = {
    layDanhMucKhoaHoc: () => api.get<DanhMucKhoaHoc[]>('/LayDanhMucKhoaHoc'),
    layKhoaHocTheoDanhMuc: (payload: QueryLayKhoaHocTheoDanhMuc) => api.get<KhoaHocTheoDanhMuc[]>(`/LayKhoaHocTheoDanhMuc?maDanhMuc=${payload.maDanhMuc}&MaNhom=${payload.maNhom}`),
    layDanhSachKhoaHoc_PhanTrang: (payload: any) => {
        const path = '/LayDanhSachKhoaHoc_PhanTrang';
        const seachString = generateSearchString({
            tenKhoaHoc: payload.tenKhoaHoc,
            page: payload.page,
            pageSize: payload.pageSize,
            maNhom: payload.maNhom,
            items: payload.items
        })
        return api.get<DanhSachKhoaHoc_PhanTrang>(path + seachString);
    },
    getListCourseSearch: (payload: string) => api.get<danhSachKhoaHoc>(`/LayDanhSachKhoaHoc?tenKhoaHoc=${payload}&MaNhom=GP01`),
    layThongTinKhoaHoc: (payload: any) => api.get<KhoaHocTheoDanhMuc>(`/LayThongTinKhoaHoc?maKhoaHoc=${payload}`),
    ghiDanhKhoaHoc: (payload: ghiDanhKH) => api.post<ghiDanhKH>(`/DangKyKhoaHoc`, payload),
    deleteGhiDanh: (payload: any) => api.post<ghiDanhKH>(`/HuyGhiDanh`, payload),
    layDanhSachKhoaHoc: (payload: string) => api.get<danhSachKhoaHoc>(`/LayDanhSachKhoaHoc?MaNhom=${payload}`),
    xoaKhoaHoc: (maKhoaHoc: string) => api.delete<danhSachKhoaHoc>(`/XoaKhoaHoc?MaKhoaHoc=${maKhoaHoc}`),
    themKhoaHoc: (payload) => api.post<danhSachKhoaHoc>(`/ThemKhoaHoc`, payload),
    updateKhoaHoc: (payload) => api.put<danhSachKhoaHoc>('/CapNhatKhoaHoc', payload),
    getInfoUser: () => api.get<listHVKH>('LayThongTinHocVienKhoaHoc'),
    getListCourseSearchAdm: (payload: string, value: string) => api.get<danhSachKhoaHoc>(`/LayDanhSachKhoaHoc?tenKhoaHoc=${payload}&MaNhom=${value}`),
    uploadHinhAnh: (formData: FormData) => 
        api.post<danhSachKhoaHoc>('/UploadHinhAnhKhoaHoc', formData)
    ,
 }
