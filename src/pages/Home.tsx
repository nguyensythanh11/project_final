import { HomeTemplate } from "components/template"

export const Home = () => {
  return (
    <HomeTemplate></HomeTemplate>
  )
}

export default Home