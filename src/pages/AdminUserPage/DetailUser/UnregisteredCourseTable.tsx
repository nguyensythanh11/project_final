import { message, Input, Table, Breakpoint } from 'antd';
import  { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { quanLyNguoiDungServices } from 'services';
import { setSpinnerOn, setSpinnerOff } from 'stores/spinnerSlice';
import { User } from 'types';
const { Search } = Input;

export const UnregisteredCourseTable = ({idUser}) => {
    const dispatch = useDispatch();
    const [listCouserUnregistered, setListCouserUnregistered] = useState([])
    console.log('sss', listCouserUnregistered);
    
    const [searchText, setSearchText] = useState('')
    const onSearch = (el) => {
        setSearchText(el.target.value.toLowerCase())
    };
    const columns = [
        {
            title: "Mã khoá học",
            dataIndex: "maKhoaHoc",
            key: "maKhoaHoc",
            width: "25%",
            responsive: ["sm"] as Breakpoint[],
        },
        {
            title: "Tên khoá học",
            dataIndex: "tenKhoaHoc",
            key: "tenKhoaHoc",
            width: "25%",
            filteredValue: [searchText],
            onFilter: (value, record) => {
            return record.tenKhoaHoc.toLowerCase().includes(value)
            }
        },
        {
            title: "Bí danh",
            dataIndex: "biDanh",
            key: "biDanh",
            width: "25%",
            responsive: ["md"] as Breakpoint[],
        },
        {
            title: "Tuỳ chọn",
            dataIndex: "action",
            key: "action",
            width: "25%",
        },
    ];

    const fetchListCouser = () => {
        dispatch(setSpinnerOn())
        quanLyNguoiDungServices.postListCourseUnregistered(idUser)
        .then((res) => {
            
            dispatch(setSpinnerOff())
            setListCouserUnregistered(res.data as User[])
        })
        .catch((err) => {
            dispatch(setSpinnerOff())
            console.log(err);
        });
    }
    useEffect(() => {
        fetchListCouser()
        // eslint-disable-next-line
    }, [])
    
    const handleAddUserToCourse = (maKhoaHoc) => {
        let data = {
            "maKhoaHoc": maKhoaHoc,
            "taiKhoan": idUser,
        }
        quanLyNguoiDungServices.getListUserCourses(data)
        .then(() => {
            toast.success(" Ghi danh thành công")
            fetchListCouser()
        })
        .catch((err) => {
            message.error(err.response.data)
            console.log(err);
        });
    }
    const dataSource = listCouserUnregistered?.map((item, index) => {
            return {
                key: index,
                maKhoaHoc: item.maKhoaHoc,
                tenKhoaHoc: item.tenKhoaHoc,
                biDanh: item.biDanh,
                action: (
                    <div>
                        <button
                        onClick={() => {handleAddUserToCourse(item.maKhoaHoc)}}
                        className='p-2 text-base text-white bg-green-500 rounded'
                        >Ghi danh</button>
                    </div>
                ),
            }
        })
  return (
    <div>
        <Search
        className='mb-1'
        placeholder="Nhập tên khóa học"
        onChange={onSearch}
        style={{
            width: 180,
        }}
        />
        <Table dataSource={dataSource} columns={columns} />
    </div>

  )
}
export default UnregisteredCourseTable
