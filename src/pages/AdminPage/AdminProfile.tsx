import { PieChartOutlined, DesktopOutlined } from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";
import { Header } from "components/ui";
import AdminUsersPage from "pages/AdminUserPage/AdminUserPage";
import { useState } from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import AdminCoursePage from "./AdminCoursePage";
import AddCourse from "./AddCourse";
import UpdateCourse from "./UpdateCourse";
import DetailCourse from "./DetailCourse/DetailCourse";
import UpdateUser from "pages/AdminUserPage/UpdateUser";
import AddUser from "pages/AdminUserPage/AddUser";
import DetailUser from "pages/AdminUserPage/DetailUser/DetailUser";
const { Content, Footer, Sider } = Layout;
function getItem(label, key, icon) {
  return {
    key,
    icon,
    // children,
    label,
  };
}
const menu = [
  getItem(
    <NavLink to={"/admin-users"}>Người dùng</NavLink>,
    "1",
    <PieChartOutlined />
  ),
  getItem(
    <NavLink to={"/admin-course"}>Khóa học</NavLink>,
    "2",
    <DesktopOutlined />
  ),
];

const AdminLayout = ( ) => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout
      style={{
        minHeight: "100vh",
        marginTop: 100,
      }}
    >
      <Sider
        breakpoint="md"
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div
          style={{
            height: 32,
            margin: 16,
            background: "rgba(255, 255, 255, 0.2)",
          }}
        />
        <Menu
          theme="dark"
          defaultSelectedKeys={["1"]}
          mode="inline"
          items={menu}
        />
      </Sider>
      <Layout className="site-layout">

        <div> <Header /> </div>
        
        <Content
          style={{
            margin: "16px",
          }}
        >
          <div
            style={{
              padding: 10,
              minHeight: 360,
              background: colorBgContainer,
            }}
          >
            <Layout className="site-layout">
              <Routes >
                <Route path="/admin-users" element={<AdminUsersPage />} />
                <Route path="/admin-course" element={<AdminCoursePage />} />
                <Route path="/admin-addcourse" element={<AddCourse />} />
                <Route path="/admin-updatecourse/:id" element={<UpdateCourse />} />
                <Route path="/admin-detailcourse/:id" element={<DetailCourse />} />
                <Route path="/admin-updateuser/:id" element={<UpdateUser />} />
                <Route path="/admin-adduser" element={<AddUser />} />
                <Route path="/admin-detailuser/:id" element={<DetailUser />} />
              </Routes>
            </Layout>
          </div>
        </Content>
        <Footer
          style={{
            textAlign: "center",
          }}
        >
          Ant Design ©2023 Created by Ant XxX
        </Footer>
      </Layout>
    </Layout>
  );
};
export default AdminLayout;
