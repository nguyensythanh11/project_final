import { message, Input, Table, Breakpoint } from 'antd';
import  { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { quanLyKhoaHocServices, quanLyNguoiDungServices} from 'services'
import { setSpinnerOn, setSpinnerOff } from 'stores/spinnerSlice';
import { User } from 'types';
const { Search } = Input;

export const UnregisteredTable = ({idCourse})  =>{
    const dispatch = useDispatch();
    const [listUserUnregistered, setListUserUnregistered] = useState<User[]>([])
    console.log('listUserUnregistered', listUserUnregistered);
    const [searchText, setSearchText] = useState('')
    const onSearch = (el) => {
        setSearchText(el.target.value.toLowerCase())
    };
    const columns = [
        {
            title: "Tài khoản",
            dataIndex: "taiKhoan",
            key: "maKhoaHoc",
            width: "25%",
            filteredValue: [searchText],
            onFilter: (value, record) => {
                const taiKhoan = record.taiKhoan ? record.taiKhoan.toLowerCase() : '';
                return taiKhoan.includes(value.toLowerCase());
              },
            responsive: ["sm"] as Breakpoint[],
        },
        {
            title: "Họ tên",
            dataIndex: "hoTen",
            key: "hinhAnh",
            width: "25%",
            responsive: ["sm"]as Breakpoint[],
        },
        {
            title: "Bí danh",
            dataIndex: "biDanh",
            key: "hinhAnh",
            width: "25%",
            responsive: ["md"] as Breakpoint[],
        },
        {
            title: "Tuỳ chọn",
            dataIndex: "action",
            key: "hinhAnh",
            width: "25%",
        },
    ];

    // danh sách học viên chưa ghi danh
    const fetchListUser = () => {
        let data = {
            "maKhoaHoc": idCourse
        }
        dispatch(setSpinnerOn());
        quanLyNguoiDungServices.listChuaGhiDanh(data)
        .then((res) => {
            dispatch(setSpinnerOff());
            setListUserUnregistered(res.data as User[])
        })
        .catch((err) => {
            dispatch(setSpinnerOff());
            console.log(err);
        });
    }
    useEffect(() => {
        fetchListUser()
        // eslint-disable-next-line
    }, [])
    

    // ghi danh khóa học
    const handleAddUserToCourse = (taiKhoan) => {
        let data = {
            "maKhoaHoc": idCourse,
            "taiKhoan": taiKhoan,
        }
        quanLyKhoaHocServices.ghiDanhKhoaHoc(data)
        .then(() => {
            message.success('Ghi danh thành công')
            fetchListUser()
        })
        .catch((err) => {
            message.error(err.response.data)
            console.log(err);
        });
    }
    const dataSource = listUserUnregistered?.map((item, index) => {
            return {
                key: index,
                taiKhoan: item.taiKhoan,
                hoTen: item.hoTen,
                biDanh: item.biDanh,
                action: (
                    <div>
                        <button
                        onClick={() => {handleAddUserToCourse(item.taiKhoan)}}
                        className='p-2 text-base text-white bg-green-500 rounded'
                        >Ghi danh</button>
                    </div>
                ),
            }
        })
  return (
    <div>
        <Search
        className='mb-1'
        placeholder="Nhập tài khoản"
        onChange={onSearch}
        style={{
            width: 180,
        }}
        />
        <Table dataSource={dataSource} columns={columns} />
    </div>

  )
}

export default UnregisteredTable