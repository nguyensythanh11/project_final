import { Input, Table, Breakpoint } from 'antd';
import  { useEffect, useState } from 'react'
import { toast } from "react-toastify";
import {  quanLyKhoaHocServices, quanLyNguoiDungServices } from 'services';
import { useAppDispatch } from 'stores';
import { setSpinnerOff, setSpinnerOn } from 'stores/spinnerSlice';
import { User } from 'types';
const { Search } = Input;

export default function RegisteredTable({idCourse}) {
  const [listUserRegistered, setListUserRegistered] = useState([])
  const [searchText, setSearchText] = useState('')

 

  const dispatch = useAppDispatch();
  const onSearch = (el) => {
    setSearchText(el.target.value.toLowerCase())
  };
  const columns = [
    {
      title: "Tài khoản",
      dataIndex: "taiKhoan",
      key: "maKhoaHoc",
      width: "25%",
      filteredValue: [searchText],
      onFilter: (value, record) => {
        const taiKhoan = record.taiKhoan ? record.taiKhoan.toLowerCase() : '';
        return taiKhoan.includes(value.toLowerCase());
      },
      responsive: ["sm"] as Breakpoint[],
    },
    {
      title: "Họ tên",
      dataIndex: "hoTen",
      key: "hinhAnh",
      width: "25%",
      responsive: ["sm"] as Breakpoint[],
    },
    {
      title: "Bí danh",
      dataIndex: "biDanh",
      key: "hinhAnh",
      width: "25%",
      responsive: ["md"] as Breakpoint[],
    },
    {
      title: "Tuỳ chọn",
      dataIndex: "action",
      key: "hinhAnh",
      width: "25%",
    },
  ];
  const fetchListUser = () => {
    let data = {
      "maKhoaHoc": idCourse
    }

    //lấy danh sách học viên đã ghi danh
    dispatch(setSpinnerOn());
    quanLyNguoiDungServices.getListUserCourses(data)
    .then((res) => {
      dispatch(setSpinnerOff());
      setListUserRegistered(res.data as User[])
    })
    .catch((err) => {
      dispatch(setSpinnerOff());
      console.log(err);
    });
  }
  useEffect(() => {
    fetchListUser();
    // eslint-disable-next-line
  }, [idCourse ])

  // hủy ghi danh
  const handleDeleteUserFromCourse = (taiKhoan) => {
    let data = {
      "maKhoaHoc": idCourse,
      "taiKhoan": taiKhoan,
    }
    quanLyKhoaHocServices.deleteGhiDanh(data)
    .then(() => {
      toast.success("Bạn đã hủy ghi danh thành công");
      fetchListUser()
    })
    .catch((err) => {
      toast.error("Hủy ghi danh không thành công")
      console.log(err);
    });
  }

  const dataSource = listUserRegistered?.map((item, index) => {
    return {
      key: index,
      taiKhoan: item.taiKhoan,
      hoTen: item.hoTen,
      biDanh: item.biDanh,
      action: (
        <div>
          <button
          onClick={() => {handleDeleteUserFromCourse(item.taiKhoan)}}
          className='p-2 text-base text-white bg-red-500 rounded'
          >Huỷ ghi danh</button>
        </div>
      ),
    }
  })

  
  return (
    <div>
      <Search
        className='mb-1'
        placeholder="Nhập tài khoản"
        onChange={onSearch}
        style={{
          width: 180,
        }}
        />
      <Table dataSource={dataSource} columns={columns} />
    </div>
  )
}
