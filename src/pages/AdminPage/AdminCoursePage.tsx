import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { FaPencilAlt, FaTrashAlt } from "react-icons/fa";
import { Select, Input, Button, message, Table , Breakpoint} from "antd";
import { useSearchParams } from "react-router-dom";
import qs from "qs";
import { setSpinnerOn, setSpinnerOff } from "stores/spinnerSlice";
import { RootState, useAppDispatch } from "stores";
import { quanLyKhoaHocServices } from "services";
import { useSelector } from "react-redux";
const { Search } = Input;

export const AdminCoursePage = ( ) => {
  const stringQuery = window.location.search.substring(1);
  let paramsObj = qs.parse(stringQuery);
  const { user } = useSelector((state: RootState) => state.quanLyNguoiDung);
  

  const dispatch = useAppDispatch();
  const [search, setSearch] = useSearchParams();
  const [isGroupCode, setIsGroupCode] = useState(() => {
    if (paramsObj.isGroupCode) {
      return paramsObj.isGroupCode;
    } else {
      return "GP01";
    }
  });
  const [listCourse, setListCourse] = useState([]);
  console.log("list", search);
  const fetchListCourse = (isGroupCode) => {
    dispatch(setSpinnerOn());
    quanLyKhoaHocServices
      .layDanhSachKhoaHoc(isGroupCode)
      .then((res) => {
        dispatch(setSpinnerOff());
        setListCourse(res.data);
      })
      .catch((err) => {
        dispatch(setSpinnerOff());
        console.log(err);
      });
  };
  useEffect(() => {
    if (paramsObj.search) {
      dispatch(setSpinnerOn());
      quanLyKhoaHocServices
        .getListCourseSearchAdm(paramsObj.search, isGroupCode)
        .then((res) => {
          dispatch(setSpinnerOff());
          setListCourse(res.data);
        })
        .catch((err) => {
          dispatch(setSpinnerOff());
          console.log(err);
        });
    } else {
      fetchListCourse(isGroupCode);
    }
  }, [isGroupCode]);

  // delete
  const handleDeleteCourse = (id) => {
    quanLyKhoaHocServices
      .xoaKhoaHoc(id)
      .then(() => {
        fetchListCourse(isGroupCode);
        message.success("Xoá thành công!");
      })
      .catch((err) => {
        console.log(err);
        message.error(err.response.data);
      });
  };


  // change groupCode
  const handleChange = (value) => {
    setIsGroupCode(value);
  };
  // search
  const onSearch = (el) => {
    let value = el.target.value;
    setSearch({
      search: value,
      isGroupCode: isGroupCode,
    });

    if (value) {
      quanLyKhoaHocServices
        .getListCourseSearchAdm(value, isGroupCode)
        .then((res) => {
          setListCourse(res.data);
        })
        .catch((err) => {
          console.log(err);
          setListCourse([]);
        });
    } else {
      fetchListCourse(isGroupCode);
    }
  };

  const dataSource = listCourse?.map((item, index) => {
    // Check if item.moTa is null or undefined, and handle it
    const truncatedMoTa = item.moTa?.substr(0, 70) || "";


    return {
      key: index,
      maKhoaHoc: item.maKhoaHoc,    
      hinhAnh: item.hinhAnh,
      tenKhoaHoc: item.tenKhoaHoc,
      tenDanhMucKhoaHoc: item.danhMucKhoaHoc.tenDanhMucKhoaHoc,
      moTa: truncatedMoTa + "...", // Use truncatedMoTa
      ngayTao: item.ngayTao,
      luotXem: item.luotXem,
      action: (
        <div>
          <div className="flex flex-col space-y-1 items-center justify-center sm:flex-row sm:space-y-0">
            <NavLink to={`/admin-updatecourse/${item.maKhoaHoc}`}>
              <button className="p-2 text-base text-white bg-amber-400 mx-1 rounded">
                <FaPencilAlt />
              </button>
            </NavLink>
            <button
              onClick={() => {
                handleDeleteCourse(item.maKhoaHoc);
              }}
              className="p-2 text-base text-white bg-red-500 mx-1 rounded"
            >
              <FaTrashAlt />
            </button>
          </div>
          <div className="flex justify-center">
            <NavLink to={`/admin-detailcourse/${item.maKhoaHoc}`}>
              <button className="p-1 text-sm mt-1 text-white bg-blue-500 mx-1 rounded">
                Xem thêm
              </button>
            </NavLink>
          </div>
        </div>
      ),
    };
  });

  const columns = [
    {
      title: "Mã khóa học",
      dataIndex: "maKhoaHoc",
      key: "maKhoaHoc",
      width: "8%",
      responsive: ["sm"] as Breakpoint[],
    },
    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
      width: "10%",
      responsive: ["md"] as Breakpoint[],
      render: (url) => {
        return <img src={url} className="w-50 h-40" alt="" />;
      },
    },
    {
      title: "Tên khóa học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
      width: "20%",
    },
    {
      title: "Danh mục",
      dataIndex: "tenDanhMucKhoaHoc",
      key: "tenDanhMucKhoaHoc",
      width: "15%",
      responsive: ["lg"] as Breakpoint[] ,
    },
    {
      title: "Mô tả",
      dataIndex: "moTa",
      key: "moTa",
      width: "20%",
      responsive: ["xl"] as Breakpoint[],
    },
    {
      title: "Ngày tạo",
      dataIndex: "ngayTao",
      key: "ngayTao",
      width: "10%",
      responsive: ["lg"] as Breakpoint[],
    },
    {
      title: "Lượt xem",
      dataIndex: "luotXem",
      key: "luotXem",
      width: "7%",
      responsive: ["md"] as Breakpoint[],
    },
    {
      title: "Tuỳ chọn",
      dataIndex: "action",
      key: "action",
      width: "10%",
      hidden: user?.maLoaiNguoiDung === "GV" ? false : true,
    },
  ].filter((item) => {
    return !item.hidden;
  });

  return (
    <div className="flex">
      <div className="pl-5 w-full">
          <div>
              {user?.maLoaiNguoiDung === "GV" ? (
                <div>
                  <NavLink to={"/admin-addcourse"}>
                    <Button type="primary" className="mb-3 bg-green-500">
                      Thêm khóa học
                    </Button>
                  </NavLink>
                </div>
              ) : (
                <></>
              )}
              <div className="mb-3 flex flex-col items-start justify-between space-y-1 sm:flex-row sm:space-y-0">
                <Select
                  defaultValue={isGroupCode}
                  style={{ width: 120 }}
                  onChange={handleChange}
                  options={[
                    { value: "GP01", label: "GP01" },
                    { value: "GP02", label: "GP02" },
                    { value: "GP03", label: "GP03" },
                    { value: "GP04", label: "GP04" },
                    { value: "GP05", label: "GP05" },
                    { value: "GP06", label: "GP06" },
                    { value: "GP07", label: "GP07" },
                    { value: "GP08", label: "GP08" },
                    { value: "GP09", label: "GP09" },
                    { value: "GP10", label: "GP10" },
                    { value: "GP11", label: "GP11" },
                    { value: "GP12", label: "GP12" },
                    { value: "GP13", label: "GP13" },
                    { value: "GP14", label: "GP14" },
                    { value: "GP15", label: "GP15" },
                  ]}
                />
                <Search
                  placeholder="Nhập tên khoá học"
                  onChange={onSearch}
                  defaultValue={paramsObj?.search}
                  style={{
                    width: 250,
                    maxWidth: "100%",
                  }}
                />
              </div>
          </div>
          <Table dataSource={dataSource} columns={columns} />
      </div>
    </div>
  );
};

export default AdminCoursePage;
