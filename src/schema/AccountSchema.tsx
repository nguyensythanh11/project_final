import { z } from 'zod';

export const AccountSchema = z.object({
    taiKhoan:   z.string()
                .nonempty('Vui lòng nhập tài khoản'),
    matKhau:    z.string()
                .nonempty('Vui lòng nhập mật khẩu'),
    hoTen:      z.string()
                .nonempty('Vui lòng nhập họ và tên'),
    soDT:       z.string()
                .nonempty('Vui lòng nhập số điện thoại'),
    email:      z.string()
                .nonempty('Vui lòng nhập email'),
    maLoaiNguoiDung:    z.string()
                        .nonempty('Vui lòng nhập loại người dùng'),
    maNhom:     z.string()
                .nonempty('Vui lòng mã nhóm'),
});

export type AccountSchemaType = z.infer<typeof AccountSchema>;


