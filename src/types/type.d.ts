declare type ApiResponses<T> = {
  statusCode: number,
  message: string,
  content: T
};