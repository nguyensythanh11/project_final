export type User = {
    key?: number
    // find(arg0: (item: User) => boolean): User
    chiTietKhoaHocGhiDanh?: ChiTietKhoaHocGhiDanh[]
    taiKhoan?: string
    matkhau?: string
    hoTen?: string
    soDT?: string
    soDt?: string
    email?: string
    maLoaiNguoiDung?: string
    maNhom?: string
    accessToken?: string
    tenLoaiNguoiDung?: string
    biDanh?: string
}

export type ChiTietKhoaHocGhiDanh = {
    maKhoaHoc?: string
    tenKhoaHoc?: string
    biDanh?: string
    moTa?: string
    luotXem?: number
    hinhAnh?: string
    ngayTao?: string
    danhGia?: number
}

export type DanhSachNguoiDung_PhanTrang = {
    currentPage?: number
    count?: number
    totalPages?: number
    totalCount?: number
    items?: User[]
}

export type QueryLayDanhSachNguoiDung_PhanTrang = {
    MaNhom?: string
    tuKhoa?: string
    page?: number
    pageSize?: number
}

export type QueryTimKiemNguoiDung = {
    MaNhom?: string
    tuKhoa?: string
}
