import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    isSpinner: false,
}

export  const spinnerSlice = createSlice({
    name: "spinnerSlice",
    initialState,
    reducers: {
        setSpinnerOn: (state) => {
            state.isSpinner = true;
        },
        setSpinnerOff: (state) => {
            state.isSpinner = false;
        },


    }
})
export const { reducer: spinnerReducer } = spinnerSlice
export const { setSpinnerOn, setSpinnerOff } = spinnerSlice.actions;

