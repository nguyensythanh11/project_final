import { configureStore } from '@reduxjs/toolkit'
import { rootReducer } from './rootReducer'
import { layThongTinNguoiDungThunk } from './quanLyNguoiDung/thunk'
import { useDispatch } from 'react-redux'


export const store = configureStore({
    reducer: rootReducer,
    devTools: true
})

const accessTokenJson = localStorage.getItem('accessToken');
if(accessTokenJson){
    store.dispatch(layThongTinNguoiDungThunk());
}

export type RootState = ReturnType<(typeof store)['getState']>

type AppDispatch = typeof store['dispatch']

export const useAppDispatch: () => AppDispatch = useDispatch
