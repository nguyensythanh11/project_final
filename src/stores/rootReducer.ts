import { combineReducers } from '@reduxjs/toolkit'
import { quanLyNguoiDungReducer } from './quanLyNguoiDung/slice'
import { quanLyKhoaHocReducer } from './quanLyKhoaHoc/slice'
import { spinnerReducer } from './spinnerSlice'

export const rootReducer = combineReducers({
    quanLyNguoiDung: quanLyNguoiDungReducer,
    quanLyKhoaHoc: quanLyKhoaHocReducer,
    spiner: spinnerReducer,
})
