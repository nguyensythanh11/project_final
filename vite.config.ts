import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    port: 3000,
  },
  resolve: {
    alias: {
      "@": path.resolve(__dirname,'./src'),
      "asset": path.resolve(__dirname,'./src/assets'),
      "components": path.resolve(__dirname,'./src/components'),
      "constant": path.resolve(__dirname,'./src/constant'),
      "hooks": path.resolve(__dirname,'./src/hooks'), 
      "pages": path.resolve(__dirname,'./src/pages'),
      "routes": path.resolve(__dirname,'./src/routes'),
      "services": path.resolve(__dirname,'./src/services'),
      "stores": path.resolve(__dirname,'./src/stores'),  
      "utils": path.resolve(__dirname,'./src/utils'), 
      "schema": path.resolve(__dirname,'./src/schema')
    }
  }
})
